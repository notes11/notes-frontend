import {ComponentFixture, TestBed} from "@angular/core/testing";

import {TopNavbarComponent} from "./top-navbar.component";
import {Router} from "@angular/router";
import {UserSessionService} from "../../../core/service/user-session/user-session.service";
import {
  UserLogoutCommandHandlerService
} from "../../../backend/user/cqrs/command/user-logout-command/user-logout-command-handler.service";
import {ToastManagerService} from "../../../core/shared/toast/service/toast-manager.service";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {DebugElement} from "@angular/core";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {user} from "../../../../test/data/test-data.spec";
import {userExpectToBe} from "../../../../test/utils/test-utils.spec";

describe("TopNavbarComponent", () => {
  let router: Router;
  let userSession: UserSessionService;
  let logoutService: UserLogoutCommandHandlerService;
  let toastManager: ToastManagerService;
  let component: TopNavbarComponent;
  let fixture: ComponentFixture<TopNavbarComponent>;
  let debugElement: DebugElement;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, ReactiveFormsModule, HttpClientTestingModule],
      declarations: [TopNavbarComponent]
    }).compileComponents();
    router = TestBed.inject(Router);
    userSession = TestBed.inject(UserSessionService);
    logoutService = TestBed.inject(UserLogoutCommandHandlerService);
    toastManager = TestBed.inject(ToastManagerService);
    fixture = TestBed.createComponent(TopNavbarComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should get current logged in user", () => {
    userSession.login(user);
    userExpectToBe(component.currentLoggedInUser(), user);
  });

  it("should logout user", () => {
    userSession.login(user);
    component.logout();
    expect(toastManager.toasts.length).toBeGreaterThan(0);
    expect(component.currentLoggedInUser()).toBeFalsy();
  });
});

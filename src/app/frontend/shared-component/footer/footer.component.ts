import {Component, OnDestroy, OnInit} from "@angular/core";
import {
  AppInfoEnvironmentQueryHandlerService
} from "../../feature/app-info/cqrs/query/app-info-environment-query-handler/app-info-environment-query-handler.service";
import {
  AppInfoEnvironmentQuery
} from "../../feature/app-info/cqrs/query/app-info-environment-query-handler/app-info-environment-query";
import {
  AppInfoVersionQueryHandlerService
} from "../../feature/app-info/cqrs/query/app-info-version-query-handler/app-info-version-query-handler.service";
import {
  AppInfoVersionQuery
} from "../../feature/app-info/cqrs/query/app-info-version-query-handler/app-info-version-query";
import {
  AppInfoBackendVersionQuery
} from "../../../backend/app-info/cqrs/query/app-info-backend-version-query-handler/app-info-backend-version-query";
import {
  AppInfoBackendVersionQueryHandlerService
} from "../../../backend/app-info/cqrs/query/app-info-backend-version-query-handler/app-info-backend-version-query-handler.service";
import {Subscription} from "rxjs";
import {
  AppInfoBackendVersionQueryResult
} from "../../../backend/app-info/cqrs/query/app-info-backend-version-query-handler/app-info-backend-version-query-result";
import {
  AppInfoBackendEnvironmentHandlerService
} from "../../../backend/app-info/cqrs/query/app-info-backend-environment-query-handler/app-info-backend-environment-handler.service";
import {
  AppInfoBackendEnvironmentQueryResult
} from "../../../backend/app-info/cqrs/query/app-info-backend-environment-query-handler/app-info-backend-environment-query-result";
import {
  AppInfoBackendEnvironmentQuery
} from "../../../backend/app-info/cqrs/query/app-info-backend-environment-query-handler/app-info-backend-environment-query";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent implements OnInit, OnDestroy {

  private appInfoEnvironmentQueryHandler: AppInfoEnvironmentQueryHandlerService;
  private appInfoVersionQueryHandler: AppInfoVersionQueryHandlerService;
  private appInfoBackendVersionQueryHandler: AppInfoBackendVersionQueryHandlerService;
  private appInfoBackendEnvironmentQueryHandler: AppInfoBackendEnvironmentHandlerService;

  private subscriptions: Subscription[] = [];

  private backendVersion: AppInfoBackendVersionQueryResult = {
    version: "N/A"
  };
  private backendEnvironment: AppInfoBackendEnvironmentQueryResult = {
    environment: "N/A"
  };

  constructor(appInfoEnvironmentQueryHandler: AppInfoEnvironmentQueryHandlerService,
              appInfoVersionQueryHandler: AppInfoVersionQueryHandlerService,
              appInfoBackendVersionQueryHandler: AppInfoBackendVersionQueryHandlerService,
              appInfoBackendEnvironmentQueryHandler: AppInfoBackendEnvironmentHandlerService) {
    this.appInfoEnvironmentQueryHandler = appInfoEnvironmentQueryHandler;
    this.appInfoVersionQueryHandler = appInfoVersionQueryHandler;
    this.appInfoBackendVersionQueryHandler = appInfoBackendVersionQueryHandler;
    this.appInfoBackendEnvironmentQueryHandler = appInfoBackendEnvironmentQueryHandler;
  }

  ngOnInit(): void {
    this.subscriptions.push(this.appInfoBackendVersionQueryHandler.execute(new AppInfoBackendVersionQuery())
      .subscribe(result => {
        this.backendVersion.version = result.version;
      }));

    this.subscriptions.push(this.appInfoBackendEnvironmentQueryHandler.execute(new AppInfoBackendEnvironmentQuery())
      .subscribe(result => {
        this.backendEnvironment.environment = result.environment;
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  public getFrontendEnvironment(): string {
    return this.appInfoEnvironmentQueryHandler.execute(new AppInfoEnvironmentQuery()).environment;
  }

  public getFrontendVersion(): string {
    return this.appInfoVersionQueryHandler.execute(new AppInfoVersionQuery()).version;
  }

  public getBackendEnvironment(): string {
    return this.backendEnvironment.environment;
  }

  public getBackendVersion(): string {
    return this.backendVersion.version;
  }


}

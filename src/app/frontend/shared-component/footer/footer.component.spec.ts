import {ComponentFixture, TestBed} from "@angular/core/testing";

import {FooterComponent} from "./footer.component";
import packageInfo from "../../../../../package.json";
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe("FooterComponent", () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [FooterComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should get frontend environment", () => {
    expect(component.getFrontendEnvironment()).toBeTruthy();
    expect(component.getFrontendEnvironment()).toBe("local");
  });

  it("should get frontend version", () => {
    expect(component.getFrontendVersion()).toBeTruthy();
    expect(component.getFrontendVersion()).toBe(packageInfo.version);
  });
});

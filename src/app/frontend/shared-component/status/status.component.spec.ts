import {ComponentFixture, TestBed} from "@angular/core/testing";

import {StatusComponent} from "./status.component";
import {DebugElement} from "@angular/core";
import {Status} from "./model/status";

describe("StatusComponent", () => {
  let component: StatusComponent;
  let fixture: ComponentFixture<StatusComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StatusComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;

    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should create - show status", () => {
    const status: Status = new Status();
    history.pushState({state: status}, "", "");
    expect(component).toBeTruthy();
    expect(component.status.state).toBe(status.state);
    expect(component.status.title).toBe(status.title);
    expect(component.status.message).toBe(status.message);
  });

  it("should be render", () => {
    expect(debugElement.nativeElement.querySelector("div")).toBeTruthy();
    expect(debugElement.nativeElement.querySelector("div>div>div>a")).toBeTruthy();
    expect(debugElement.nativeElement.querySelector("div>div>div>a").textContent).toContain("Go to home page");
  });
});

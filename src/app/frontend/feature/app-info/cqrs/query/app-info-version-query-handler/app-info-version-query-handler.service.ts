import {Injectable} from "@angular/core";
import {QueryHandler} from "../../../../../../core/cqrs/query/handler/query-handler";
import {AppInfoVersionQuery} from "./app-info-version-query";
import {AppInfoVersionQueryResult} from "./app-info-version-query-result";
import packageInfo from "../../../../../../../../package.json";

@Injectable({
  providedIn: "root"
})
export class AppInfoVersionQueryHandlerService implements QueryHandler<AppInfoVersionQuery, AppInfoVersionQueryResult> {
  execute(query: AppInfoVersionQuery): AppInfoVersionQueryResult {
    return {
      version: packageInfo.version
    };
  }
}

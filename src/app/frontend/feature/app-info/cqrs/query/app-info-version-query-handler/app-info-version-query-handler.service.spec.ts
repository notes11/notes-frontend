import {TestBed} from "@angular/core/testing";

import {AppInfoVersionQueryHandlerService} from "./app-info-version-query-handler.service";
import {AppInfoVersionQuery} from "./app-info-version-query";
import packageInfo from "../../../../../../../../package.json";

describe("AppInfoVersionQueryHandlerService", () => {
  let service: AppInfoVersionQueryHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppInfoVersionQueryHandlerService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should get version", () => {
    expect(service.execute(new AppInfoVersionQuery())).toBeTruthy();
    expect(service.execute(new AppInfoVersionQuery()).version).toBe(packageInfo.version);
  });
});

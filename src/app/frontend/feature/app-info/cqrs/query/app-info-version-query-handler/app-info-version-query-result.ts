import {QueryResult} from "../../../../../../core/cqrs/query/result/query-result";

export class AppInfoVersionQueryResult implements QueryResult {
  public version: string;
}

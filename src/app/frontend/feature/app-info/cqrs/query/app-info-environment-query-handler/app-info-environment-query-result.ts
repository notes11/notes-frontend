import {QueryResult} from "../../../../../../core/cqrs/query/result/query-result";

export class AppInfoEnvironmentQueryResult implements QueryResult {

  public environment: string;

}

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";

import { NoteListComponent } from "./note-list.component";
import {ToastManagerService} from "../../../../core/shared/toast/service/toast-manager.service";
import {ConfigurationService} from "../../../../core/service/app-config/configuration.service";
import {notePageResponse} from "../../../../../test/data/test-data.spec";
import {appRoutes} from "../../../../routes";

describe("NoteListComponent", () => {
  let component: NoteListComponent;
  let fixture: ComponentFixture<NoteListComponent>;
  let httpMock: HttpTestingController;
  let configuration: ConfigurationService;
  let toastManager: ToastManagerService;
  let noteUrlBackend: string = "";

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule, FormsModule, RouterTestingModule.withRoutes(appRoutes), HttpClientTestingModule
      ],
      declarations: [NoteListComponent],
    }).compileComponents();
    // noinspection DuplicatedCode
    configuration = TestBed.inject(ConfigurationService);
    noteUrlBackend = "";
    noteUrlBackend += configuration.getConfiguration().backend.apiBaseUri;
    noteUrlBackend += configuration.getConfiguration().backend.apiBasePath;
    noteUrlBackend += configuration.getConfiguration().backend.apiMappings.note;

    toastManager = TestBed.inject(ToastManagerService);
    httpMock = TestBed.inject(HttpTestingController);
    fixture = TestBed.createComponent(NoteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should create and failed on load notes", () => {
    // given
    const request = httpMock.expectOne(`${noteUrlBackend}?size=5&page=0&direction=DESC`);
    expect(request.request.method).toBe("GET");
    request.flush("", {
      status: 500,
      statusText: "Internal Server Error"
    });
    // then
    expect(toastManager.toasts.length).toBeGreaterThanOrEqual(1);
    expect(toastManager.toasts[0].body).toBe("Failed to load notes");
  });

  it("should create and load notes", () => {
    // given
    const request = httpMock.expectOne(`${noteUrlBackend}?size=5&page=0&direction=DESC`);
    expect(request.request.method).toBe("GET");
    request.flush(notePageResponse);
    // then
    expect(toastManager.toasts.length).toBeGreaterThanOrEqual(1);
    expect(toastManager.toasts[0].body).toBe("Notes have been loaded.");
    expect(component.noteQueryResult).toBeDefined();
  });

  it("should getCurrentPage", () => {
    // given
    const request = httpMock.expectOne(`${noteUrlBackend}?size=5&page=0&direction=DESC`);
    expect(request.request.method).toBe("GET");
    request.flush(notePageResponse);
    const expectedCurrentPage = 1;
    // when & then
    expect(component.getCurrentPage()).toBe(expectedCurrentPage);
  });

  it("should getCurrentPage when empty response has been provided", () => {
    // given
    const request = httpMock.expectOne(`${noteUrlBackend}?size=5&page=0&direction=DESC`);
    expect(request.request.method).toBe("GET");
    request.flush("");
    const expectedCurrentPage = 0;
    // when & then
    expect(component.getCurrentPage()).toBe(expectedCurrentPage);
  });

  it("should numSequence without parameter", () => {
    // given
    const request = httpMock.expectOne(`${noteUrlBackend}?size=5&page=0&direction=DESC`);
    expect(request.request.method).toBe("GET");
    request.flush(notePageResponse);
    // when & then
    expect(component.numSequence()).toEqual([]);
  });

  it("should numSequence with parameter", () => {
    // given
    const request = httpMock.expectOne(`${noteUrlBackend}?size=5&page=0&direction=DESC`);
    expect(request.request.method).toBe("GET");
    request.flush(notePageResponse);
    // when & then
    expect(component.numSequence(5)).toEqual([0, 1, 2, 3, 4]);
  });

  it("should change changePageSize", () => {
    // given
    const request = httpMock.expectOne(`${noteUrlBackend}?size=5&page=0&direction=DESC`);
    expect(request.request.method).toBe("GET");
    request.flush(notePageResponse);
    // when
    component.changePageSize(component.noteQuery, component.noteQueryResult, 2);
    // then
    expect(component.noteQuery.size).toBe(2);
  });

  it("should change to previousPage", () => {
    // given
    const request = httpMock.expectOne(`${noteUrlBackend}?size=5&page=0&direction=DESC`);
    expect(request.request.method).toBe("GET");
    request.flush(notePageResponse);
    // when
    component.previousPage(component.noteQuery, component.noteQueryResult);
    // then
    expect(component.noteQuery.page).toBe(0);
  });

  it("should change to nextPage", () => {
    // given
    const request = httpMock.expectOne(`${noteUrlBackend}?size=5&page=0&direction=DESC`);
    expect(request.request.method).toBe("GET");
    request.flush(notePageResponse);
    // when
    component.nextPage(component.noteQuery, component.noteQueryResult);
    // then
    expect(component.noteQuery.page).toBe(1);
  });

  it("should change page", () => {
    // given
    const request = httpMock.expectOne(`${noteUrlBackend}?size=5&page=0&direction=DESC`);
    expect(request.request.method).toBe("GET");
    request.flush(notePageResponse);
    // when
    component.changePage(component.noteQuery, component.noteQueryResult, 1);
    // then
    expect(component.noteQuery.page).toBe(1);
  });
});

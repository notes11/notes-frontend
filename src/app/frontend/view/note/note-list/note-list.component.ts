import {Component, OnDestroy, OnInit} from "@angular/core";
import {NoteQueryResult} from "../../../../backend/note/cqrs/query/note-query/note-query-result";
import {NoteQueryHandlerService} from "../../../../backend/note/cqrs/query/note-query/note-query-handler.service";
import {NoteQuery} from "../../../../backend/note/cqrs/query/note-query/note-query";
import {ToastManagerService} from "../../../../core/shared/toast/service/toast-manager.service";
import {environment} from "../../../../../environments/environment";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {FormBuilder, FormGroup} from "@angular/forms";
import {
  NoteDeleteCommandHandlerService
} from "../../../../backend/note/cqrs/command/note-delete/note-delete-command-handler.service";

@Component({
  selector: "app-note-list",
  templateUrl: "./note-list.component.html",
  styleUrls: ["./note-list.component.scss"],
})
export class NoteListComponent implements OnInit, OnDestroy {
  public appMappings = environment.appMapping;
  public formGroup: FormGroup;
  public noteQuery: NoteQuery;
  public noteQueryResult: NoteQueryResult;
  private subscription: Subscription = new Subscription();

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private noteQueryHandlerService: NoteQueryHandlerService,
    private toastManager: ToastManagerService,
    private noteDeleteCommandHandlerService: NoteDeleteCommandHandlerService,) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((queryParameters) => {
      this.noteQuery = new NoteQuery(queryParameters);
    });

    this.initFilterForm();

    this.subscription = this.noteQueryHandlerService
      .execute(this.noteQuery)
      .subscribe({
        next: (response) => {
          this.noteQueryResult = response;
          this.toastManager.showSuccess("Notes have been loaded.");
        },
        error: () => {
          this.toastManager.showDanger("Failed to load notes");
        },
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public onFilterChange(): void {
    if (this.formGroup.valid) {
      this.noteQuery.createdBy = this.formGroup.controls.createdBy.value;
      this.noteQuery.createdTimestampAfter = this.formGroup.controls.createdTimestampAfter.value;
      this.noteQuery.createdTimestampBefore = this.formGroup.controls.createdTimestampBefore.value;

      this.noteQuery.modifiedBy = this.formGroup.controls.modifiedBy.value;
      this.noteQuery.modifiedTimestampAfter = this.formGroup.controls.modifiedTimestampAfter.value;
      this.noteQuery.modifiedTimestampBefore = this.formGroup.controls.modifiedTimestampBefore.value;

      this.noteQuery.title = this.formGroup.controls.title.value;

      const url: string = environment.appMapping.note.noteList;
      this.router
        .navigate([url], {
          queryParams: this.noteQuery,
        })
        .then(() => this.ngOnInit());
    }
  }

  public numSequence(numSequence?: number): Array<number> {
    if (numSequence) {
      const array: Array<number> = new Array(numSequence);
      for (let i = 0; i < array.length; i++) {
        array[i] = i;
      }
      return array;
    }
    return [];
  }

  public changePageSize(noteQuery: NoteQuery, noteQueryResult: NoteQueryResult, pageSize: number): void {
    noteQuery.changePageSize(pageSize, noteQueryResult);
    this.refreshPage(noteQuery);
  }

  public previousPage(noteQuery: NoteQuery, noteQueryResult: NoteQueryResult): void {
    noteQuery.previousPage();
    this.refreshPage(noteQuery);
  }

  public nextPage(noteQuery: NoteQuery, noteQueryResult: NoteQueryResult): void {
    noteQuery.nextPage(noteQueryResult);
    this.refreshPage(noteQuery);
  }

  public changePage(noteQuery: NoteQuery, noteQueryResult: NoteQueryResult, pageNumber: number): void {
    noteQuery.changePage(pageNumber, noteQueryResult);
    this.refreshPage(noteQuery);
  }

  public getCurrentPage(): number {
    if (this.noteQueryResult) {
      if (this.noteQueryResult.totalPages > 0) {
        return this.noteQueryResult.number + 1;
      }
    }
    return 0;
  }

  public delete(noteQuery: NoteQuery, uuid: string): void {
    this.noteDeleteCommandHandlerService.execute({uuid})
      .subscribe({
        next: () => {
          this.toastManager.showSuccess("Note has been successfully deleted.");
        },
        error: () => {
          this.toastManager.showDanger("Failed to delete a note. Please try again later.");
        },
        complete: () => {
          this.refreshPage(noteQuery);
        }
      });
  }

  private refreshPage(noteQuery: NoteQuery): void {
    const url: string = environment.appMapping.note.noteList;
    this.router.navigate([url], {queryParams: noteQuery}).then(() => this.ngOnInit());
  }

  private initFilterForm(): void {
    this.formGroup = this.formBuilder.group({
      createdBy: [this.noteQuery.createdBy],
      createdTimestampAfter: [this.noteQuery.createdTimestampAfter],
      createdTimestampBefore: [this.noteQuery.createdTimestampBefore],
      modifiedBy: [this.noteQuery.modifiedBy],
      modifiedTimestampAfter: [this.noteQuery.modifiedTimestampAfter],
      modifiedTimestampBefore: [this.noteQuery.modifiedTimestampBefore],
      title: [this.noteQuery.title]
    });

    if (this.noteQuery.title) {
      this.formGroup.controls.title.setValue(this.noteQuery.title);
    }
  }

}

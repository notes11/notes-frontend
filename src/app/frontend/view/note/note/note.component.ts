import {Component, OnDestroy, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {NoteAddCommand} from "src/app/backend/note/cqrs/command/add-command-handler/note-add-command";
import {
  NoteAddCommandHandlerService
} from "src/app/backend/note/cqrs/command/add-command-handler/note-add-command-handler.service";
import {NoteUpdateCommand} from "src/app/backend/note/cqrs/command/note-update/note-update-command";
import {
  NoteUpdateCommandHandlerServiceService
} from "src/app/backend/note/cqrs/command/note-update/note-update-command-handler-service.service";
import {NoteQuery} from "src/app/backend/note/cqrs/query/note-query/note-query";
import {NoteQueryHandlerService} from "src/app/backend/note/cqrs/query/note-query/note-query-handler.service";
import {NoteQueryResult} from "src/app/backend/note/cqrs/query/note-query/note-query-result";
import {ToastManagerService} from "../../../../core/shared/toast/service/toast-manager.service";
import {environment} from "../../../../../environments/environment";
import {
  NoteDeleteCommandHandlerService
} from "../../../../backend/note/cqrs/command/note-delete/note-delete-command-handler.service";

@Component({
  selector: "app-note",
  templateUrl: "./note.component.html",
  styleUrls: ["./note.component.scss"],
})
export class NoteComponent implements OnInit, OnDestroy {

  public appMapping = environment.appMapping;

  public noteFormGroup: FormGroup;
  public isLoading: boolean = false;
  public isEditing: boolean = false;

  public noteQueryResult: NoteQueryResult;
  public noteQuery: NoteQuery;

  public addNoteCommand: NoteAddCommand = new NoteAddCommand();
  public noteUpdateCommand: NoteUpdateCommand = new NoteUpdateCommand();

  private subscription: Subscription = new Subscription();

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder,
              private noteQueryHandlerService: NoteQueryHandlerService,
              private addNoteCommandHandlerService: NoteAddCommandHandlerService,
              private noteUpdateCommandHandlerService: NoteUpdateCommandHandlerServiceService,
              private noteDeleteCommandHandlerService: NoteDeleteCommandHandlerService,
              private toastManager: ToastManagerService) {
  }

  ngOnInit(): void {
    this.isLoading = false;
    this.isEditing = false;
    this.initForm();

    this.activatedRoute.params.subscribe((params) => {
      if (params.uuid) {
        this.noteQuery = new NoteQuery(params);
        this.noteQuery.changePageSize(1);
        this.subscription = this.noteQueryHandlerService
          .execute(this.noteQuery)
          .subscribe({
            next: (result) => {
              if (result.content && result.content.length > 0) {
                this.noteQueryResult = result;

                this.addNoteCommand.title = this.noteQueryResult.content[0].title;
                this.addNoteCommand.noteText = this.noteQueryResult.content[0].noteText;
                this.noteUpdateCommand.title = this.noteQueryResult.content[0].title;
                this.noteUpdateCommand.noteText = this.noteQueryResult.content[0].noteText;

                this.isEditing = true;
              } else {
                this.handleNotFoundNote(params);
              }
            },
            error: () => this.handleNotFoundNote(params)
          });
      } else {
        this.noteQueryResult = new NoteQueryResult();
        this.noteQueryResult.content.push({
          uuid: undefined,
          createdBy: undefined,
          createdTimestamp: undefined,
          modifiedBy: undefined,
          modificationTimestamp: undefined,
          title: undefined,
          noteText: undefined,
          owner: null
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public onSubmit(): void {
    if (this.noteFormGroup.valid) {
      this.isLoading = true;
      if (this.isEditing) {
        this.handleNoteUpdate();
      } else {
        this.handleNoteAdd();
      }
    }
  }

  public deleteNote(noteQueryResult: NoteQueryResult): void {
    const url: string = this.appMapping.note.noteList;
    this.noteDeleteCommandHandlerService.execute({uuid: noteQueryResult.content[0].uuid})
      .subscribe({
        next: () => {
          this.toastManager.showSuccess("Note has been successfully deleted.");
        },
        error: () => {
          this.toastManager.showDanger("Failed to delete a note. Please try again later.");
          this.isLoading = false;
        },
        complete: () => {
          this.router.navigateByUrl(url).then();
        }
      });
  }

  private initForm() {
    this.noteFormGroup = this.formBuilder.group({
      title: [this.addNoteCommand.title, [Validators.required, Validators.minLength(5), Validators.maxLength(255)]],
      noteText: [this.addNoteCommand.noteText, [Validators.required, Validators.minLength(2)]]
    });
  }

  private handleNotFoundNote(params: Params): void {
    this.toastManager.showDanger(`Failed to load note - '${params.uuid}'`);
    this.router.navigateByUrl("").then();
  }

  private handleNoteUpdate(): void {
    const url: string = this.router.url;
    this.noteUpdateCommand.uuid = this.noteQueryResult.content[0].uuid;
    this.noteUpdateCommandHandlerService.execute(this.noteUpdateCommand)
      .subscribe({
        next: () => {
          this.toastManager.showSuccess("Note has been successfully updated.");
        },
        error: () => {
          this.toastManager.showDanger("Failed to update a note. Please try again later.");
          this.isLoading = false;
        },
        complete: () => {
          this.router.navigateByUrl(url).then(() => this.ngOnInit());
        }
      });
  }

  private handleNoteAdd(): void {
    let url: string = this.router.url;
    this.addNoteCommandHandlerService.execute(this.addNoteCommand).subscribe({
      next: result => {
        this.toastManager.showSuccess("Note has been saved successfully.");
        url += `/${result.uuid}`;
      },
      error: () => {
        this.toastManager.showDanger("Failed to save a note. Please try again later.");
        this.isLoading = false;
      },
      complete: () => {
        this.router.navigateByUrl(url).then();
      }
    });
  }
}

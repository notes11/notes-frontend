import {ComponentFixture, TestBed} from "@angular/core/testing";

import {UserRegisterComponent} from "./user-register.component";
import {Router} from "@angular/router";
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {
  UserRegisterCommandHandlerService
} from "../../../../backend/user/cqrs/command/user-register-command/user-register-command-handler.service";
import {ToastManagerService} from "../../../../core/shared/toast/service/toast-manager.service";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {DebugElement} from "@angular/core";
import {ConfigurationService} from "../../../../core/service/app-config/configuration.service";
import {userRegisterCommand, userRegisterCommandResult} from "../../../../../test/data/test-data.spec";

describe("UserRegisterComponent", () => {
  let component: UserRegisterComponent;
  let fixture: ComponentFixture<UserRegisterComponent>;
  let debugElement: DebugElement;
  let router: Router;
  let formBuilder: FormBuilder;
  let registerService: UserRegisterCommandHandlerService;
  let toastManagerService: ToastManagerService;

  let configurationService: ConfigurationService;
  let registerTestUrl: string;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, ReactiveFormsModule, FormsModule, HttpClientTestingModule]
    }).compileComponents();
    configurationService = TestBed.inject(ConfigurationService);
    registerTestUrl = "";
    registerTestUrl += configurationService.getConfiguration().backend.apiBaseUri;
    registerTestUrl += configurationService.getConfiguration().backend.apiBasePath;
    registerTestUrl += configurationService.getConfiguration().backend.apiMappings.user;

    httpMock = TestBed.inject(HttpTestingController);
    router = TestBed.inject(Router);
    formBuilder = TestBed.inject(FormBuilder);
    registerService = TestBed.inject(UserRegisterCommandHandlerService);
    toastManagerService = TestBed.inject(ToastManagerService);
    fixture = TestBed.createComponent(UserRegisterComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should submit", () => {
    const username: HTMLInputElement = debugElement.nativeElement.querySelector("input[id='username']");
    expect(username).toBeTruthy();
    const addressEmail: HTMLInputElement = debugElement.nativeElement.querySelector("input[id='addressEmail']");
    expect(addressEmail).toBeTruthy();
    const password: HTMLInputElement = debugElement.nativeElement.querySelector("input[id='password']");
    expect(password).toBeTruthy();
    const passwordRepeat: HTMLInputElement = debugElement.nativeElement.querySelector("input[id='passwordRepeat']");
    expect(passwordRepeat).toBeTruthy();

    component.userCreateForm.controls.username.setValue(userRegisterCommand.username);
    component.userCreateForm.controls.addressEmail.setValue(userRegisterCommand.addressEmail);
    component.userCreateForm.controls.password.setValue(userRegisterCommand.password);
    component.userCreateForm.controls.passwordRepeat.setValue(userRegisterCommand.passwordRepeat);
    expect(component.userCreateForm.valid).toBeTruthy();

    component.onSubmit(component.userCreateForm);
    expect(component.submitted).toBeTruthy();

    const request = httpMock.expectOne(registerTestUrl);
    expect(request.request.method).toBe("POST");
    request.flush(userRegisterCommandResult);

    expect(toastManagerService.toasts.length).toBeGreaterThan(0);
  });
});

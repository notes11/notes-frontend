import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {first} from "rxjs/operators";
import {ToastManagerService} from "../../../../core/shared/toast/service/toast-manager.service";
import {
  UserLoginCommandHandlerService
} from "../../../../backend/user/cqrs/command/user-login-command/user-login-command-handler.service";

@Component({
  selector: "app-user-login",
  templateUrl: "./user-login.component.html",
  styleUrls: ["./user-login.component.scss"]
})
export class UserLoginComponent implements OnInit {

  public loginFormGroup: FormGroup;
  public loading = false;
  public submitted = false;
  public errorMessage: string;

  private formBuilder: FormBuilder;
  private activatedRoute: ActivatedRoute;
  private router: Router;
  private loginService: UserLoginCommandHandlerService;
  private toastService: ToastManagerService;

  constructor(formBuilder: FormBuilder,
              activatedRoute: ActivatedRoute,
              router: Router,
              loginService: UserLoginCommandHandlerService,
              toastService: ToastManagerService) {
    this.formBuilder = formBuilder;
    this.activatedRoute = activatedRoute;
    this.router = router;
    this.loginService = loginService;
    this.toastService = toastService;
  }

  ngOnInit(): void {
    this.loginFormGroup = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.loginFormGroup.invalid) {
      return;
    }

    this.loading = true;
    this.loginService.execute({
      username: this.loginFormGroup.controls.username.value,
      password: this.loginFormGroup.controls.password.value
    })
      .pipe(first())
      .subscribe(() => {
          this.toastService.showSuccess("Logged in successful");
          // noinspection JSIgnoredPromiseFromCall
          this.router.navigateByUrl("/");
        },
        error => {
          this.errorMessage = error;
          this.loading = false;
          this.toastService.showDanger("Failed to login");
        });
  }

}

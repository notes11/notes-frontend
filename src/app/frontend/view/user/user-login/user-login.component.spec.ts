import {ComponentFixture, TestBed} from "@angular/core/testing";

import {UserLoginComponent} from "./user-login.component";
import {RouterTestingModule} from "@angular/router/testing";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {UserSessionService} from "../../../../core/service/user-session/user-session.service";
import {ToastManagerService} from "../../../../core/shared/toast/service/toast-manager.service";
import {Router} from "@angular/router";
import {ConfigurationService} from "../../../../core/service/app-config/configuration.service";
import {userLoginCommand, userLoginCommandResult} from "../../../../../test/data/test-data.spec";

describe("UserLoginComponent", () => {
  let component: UserLoginComponent;
  let fixture: ComponentFixture<UserLoginComponent>;
  let userSessionService: UserSessionService;
  let httpMock: HttpTestingController;
  let toastManagerService: ToastManagerService;
  let router: Router;

  let configurationService: ConfigurationService;
  let loginUrl: string;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, ReactiveFormsModule, FormsModule, HttpClientTestingModule],
      declarations: [UserLoginComponent]
    }).compileComponents();
    configurationService = TestBed.inject(ConfigurationService);
    loginUrl = "";
    loginUrl += configurationService.getConfiguration().backend.apiBaseUri;
    loginUrl += configurationService.getConfiguration().backend.apiBasePath;
    loginUrl += configurationService.getConfiguration().backend.apiMappings.login;

    router = TestBed.inject(Router);
    toastManagerService = TestBed.inject(ToastManagerService);
    httpMock = TestBed.inject(HttpTestingController);
    userSessionService = TestBed.inject(UserSessionService);
    fixture = TestBed.createComponent(UserLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    userSessionService.logout();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should not login on invalid form", () => {
    // when
    component.onSubmit();
    // then
    expect(component.submitted).toBeTruthy();
    expect(userSessionService.isUserLoggedIn()).toBeFalsy();
  });

  it("should login", () => {
    // given
    component.loginFormGroup.controls.username.setValue(userLoginCommand.username);
    component.loginFormGroup.controls.password.setValue(userLoginCommand.password);
    expect(component.loginFormGroup.valid).toBeTruthy();
    const navigateSpy = spyOn(router, "navigateByUrl");

    // when
    component.onSubmit();

    const request = httpMock.expectOne(loginUrl);
    expect(request.request.method).toBe("POST");
    request.flush(userLoginCommandResult);

    // then
    expect(component.submitted).toBeTruthy();
    expect(toastManagerService.toasts.length).toBeGreaterThan(0);
    expect(toastManagerService.toasts[0].body).toBe("Logged in successful");
    expect(navigateSpy).toHaveBeenCalledWith("/");
  });

  it("should fail on login", () => {
    // given
    component.loginFormGroup.controls.username.setValue(userLoginCommand.username);
    component.loginFormGroup.controls.password.setValue(userLoginCommand.password);
    expect(component.loginFormGroup.valid).toBeTruthy();

    // when
    component.onSubmit();

    const request = httpMock.expectOne(loginUrl);
    expect(request.request.method).toBe("POST");
    request.flush(userLoginCommandResult, {
      status: 401,
      statusText: "Unauthorized"
    });

    // then
    expect(component.submitted).toBeTruthy();
    expect(component.loading).toBeFalsy();
    expect(toastManagerService.toasts.length).toBeGreaterThan(0);
    expect(toastManagerService.toasts[0].body).toBe("Failed to login");
  });
});

import {Command} from "../../../../../core/cqrs/command/command";

export class NoteAddCommand implements Command {

  public title: string;
  public noteText: string;

}

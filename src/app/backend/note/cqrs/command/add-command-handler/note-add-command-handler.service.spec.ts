import {TestBed} from "@angular/core/testing";

import {NoteAddCommandHandlerService} from "./note-add-command-handler.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";
import {noteAddCommand, noteAddCommandResult} from "../../../../../../test/data/test-data.spec";
import {noteExpectToBe} from "../../../../../../test/utils/test-utils.spec";

describe("NoteAddCommandHandlerService", () => {
  let service: NoteAddCommandHandlerService;
  let httpMock: HttpTestingController;

  let configurationService: ConfigurationService;
  let createNoteUrl: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    configurationService = TestBed.inject(ConfigurationService);
    createNoteUrl = "";
    createNoteUrl += configurationService.getConfiguration().backend.apiBaseUri;
    createNoteUrl += configurationService.getConfiguration().backend.apiBasePath;
    createNoteUrl += configurationService.getConfiguration().backend.apiMappings.note;

    httpMock = TestBed.inject(HttpTestingController);
    service = TestBed.inject(NoteAddCommandHandlerService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should add note (create new note)", () => {
    service.execute(noteAddCommand).subscribe(result => {
      noteExpectToBe(result, noteAddCommandResult);
    });

    const request = httpMock.expectOne(createNoteUrl);
    expect(request.request.method).toBe("POST");
    request.flush(noteAddCommandResult, {
      status: 201,
      statusText: "Created"
    });
  });
});

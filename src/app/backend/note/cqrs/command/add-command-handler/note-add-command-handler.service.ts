import {Injectable} from "@angular/core";
import {AbstractCommandHandler} from "../../../../../core/cqrs/command/handler/abstract-command-handler.service";
import {NoteAddCommand} from "./note-add-command";
import {NoteAddCommandResult} from "./note-add-command-result";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export class NoteAddCommandHandlerService extends AbstractCommandHandler<NoteAddCommand, NoteAddCommandResult> {

  private readonly apiBaseNotePath: string;

  constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
    this.apiBaseNotePath = this.configurationService.getConfiguration().backend.apiMappings.note;
  }

  execute(command: NoteAddCommand): Observable<NoteAddCommandResult> {
    return this.httpClient.post<NoteAddCommandResult>(this.apiBaseUriWithBasePath + this.apiBaseNotePath, command);
  }
}

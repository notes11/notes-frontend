import {Note} from "../../../model/dto/note";
import {CommandResult} from "../../../../core/cqrs/command/result/command-result";

export class NoteCommandResult extends Note implements CommandResult {
}

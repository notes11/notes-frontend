import {Injectable} from "@angular/core";
import {AbstractCommandHandler} from "../../../../../core/cqrs/command/handler/abstract-command-handler.service";
import {NoteDeleteCommand} from "./note-delete-command";
import {NoteDeleteCommandResult} from "./note-delete-command-result";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export class NoteDeleteCommandHandlerService extends AbstractCommandHandler<NoteDeleteCommand, NoteDeleteCommandResult> {

  private readonly apiBaseNotePath: string;

  constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
    this.apiBaseNotePath = this.configurationService.getConfiguration().backend.apiMappings.note;
  }

  execute(command: NoteDeleteCommand): Observable<NoteDeleteCommandResult> {
    const fullUrl = `${this.apiBaseUriWithBasePath}${this.apiBaseNotePath}/${command.uuid}`;
    console.log(fullUrl);
    return this.httpClient.delete<NoteDeleteCommandResult>(fullUrl);
  }
}

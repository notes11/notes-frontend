import {TestBed} from "@angular/core/testing";

import {NoteDeleteCommandHandlerService} from "./note-delete-command-handler.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";
import {
  noteDeleteCommand,
  noteDeleteCommandResult
} from "../../../../../../test/data/test-data.spec";

describe("NoteDeleteCommandHandlerService", () => {
  let service: NoteDeleteCommandHandlerService;
  let deleteUrl: string;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpMock = TestBed.inject(HttpTestingController);
    const configurationService: ConfigurationService = TestBed.inject(ConfigurationService);
    service = TestBed.inject(NoteDeleteCommandHandlerService);

    deleteUrl = "";
    deleteUrl += configurationService.getConfiguration().backend.apiBaseUri;
    deleteUrl += configurationService.getConfiguration().backend.apiBasePath;
    deleteUrl += configurationService.getConfiguration().backend.apiMappings.note;
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should delete note", () => {
    // given
    deleteUrl += `/${noteDeleteCommand.uuid}`;

    // when & then
    service.execute(noteDeleteCommand).subscribe({
      next: result => {
        expect(result).toBeDefined();
        expect(result).toBeTruthy();
      }
    });

    const request = httpMock.expectOne(deleteUrl);
    expect(request.request.method).toBe("DELETE");
    request.flush(noteDeleteCommandResult);
  });
});

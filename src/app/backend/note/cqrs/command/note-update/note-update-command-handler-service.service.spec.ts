import { TestBed } from "@angular/core/testing";

import { NoteUpdateCommandHandlerServiceService } from "./note-update-command-handler-service.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";
import {noteUpdateCommand, noteUpdateCommandResult} from "../../../../../../test/data/test-data.spec";

describe("NoteUpdateCommandHandlerServiceService", () => {
  let service: NoteUpdateCommandHandlerServiceService;
  let updateUrl: string;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpMock = TestBed.inject(HttpTestingController);
    const configurationService: ConfigurationService = TestBed.inject(ConfigurationService);
    service = TestBed.inject(NoteUpdateCommandHandlerServiceService);

    updateUrl = "";
    updateUrl += configurationService.getConfiguration().backend.apiBaseUri;
    updateUrl += configurationService.getConfiguration().backend.apiBasePath;
    updateUrl += configurationService.getConfiguration().backend.apiMappings.note;
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should update note", () => {
    // given
    updateUrl += `/${noteUpdateCommand.uuid}`;

    // when & then
    service.execute(noteUpdateCommand).subscribe({
      next: result => {
        expect(result).toBeTruthy();
        expect(result.uuid).toEqual(noteUpdateCommandResult.uuid);
        expect(result.createdBy).toEqual(noteUpdateCommandResult.createdBy);
        expect(result.createdTimestamp).toEqual(noteUpdateCommandResult.createdTimestamp);
        expect(result.modifiedBy).toEqual(noteUpdateCommandResult.modifiedBy);
        expect(result.modificationTimestamp).toEqual(noteUpdateCommandResult.modificationTimestamp);
        expect(result.title).toEqual(noteUpdateCommandResult.title);
        expect(result.noteText).toEqual(noteUpdateCommandResult.noteText);
      }
    });

    const request = httpMock.expectOne(updateUrl);
    expect(request.request.method).toBe("PUT");
    request.flush(noteUpdateCommandResult);
  });
});

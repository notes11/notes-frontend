import {QueryResult} from "../../../../../core/cqrs/query/result/query-result";
import {Page} from "../../../../model/other/page";
import {Note} from "../../../../model/dto/note";

export class NoteQueryResult extends Page<Note> implements QueryResult {
}

import { Params } from "@angular/router";
import { PageRequest } from "src/app/backend/model/other/page-request";
import { Query } from "src/app/core/cqrs/query/query";

export class BaseQuery extends PageRequest implements Query {
  public uuid: string;
  public createdBy: string;
  public createdTimestampAfter: Date;
  public createdTimestampBefore: Date;

  public modifiedBy: string;
  public modifiedTimestampAfter: Date;
  public modifiedTimestampBefore: Date;

  constructor(params?: Params) {
    super(params);

    if (params) {
      if (params.uuid) {
        this.uuid = params.uuid;
      }

      if (params.createdBy) {
        this.createdBy = params.createdBy;
      }
      if (params.createdTimestampAfter) {
        this.createdTimestampAfter = params.createdTimestampAfter;
      }
      if (params.createdTimestampBefore) {
        this.createdTimestampBefore = params.createdTimestampBefore;
      }

      if (params.modifiedBy) {
        this.modifiedBy = params.modifiedBy;
      }
      if (params.modifiedTimestampAfter) {
        this.modifiedTimestampAfter = params.modifiedTimestampAfter;
      }
      if (params.modifiedTimestampBefore) {
        this.modifiedTimestampBefore = params.modifiedTimestampBefore;
      }
    }
  }
}

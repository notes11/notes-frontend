import {TestBed} from "@angular/core/testing";

import {UserLogoutCommandHandlerService} from "./user-logout-command-handler.service";
import {HttpClient} from "@angular/common/http";
import {UserSessionService} from "../../../../../core/service/user-session/user-session.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {UserLogoutCommand} from "./user-logout-command";

describe("UserLogoutCommandHandlerService", () => {
  let service: UserLogoutCommandHandlerService;
  let userSession: UserSessionService;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpClient = TestBed.inject(HttpClient);
    userSession = TestBed.inject(UserSessionService);
    service = TestBed.inject(UserLogoutCommandHandlerService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should be logged-out", () => {
    expect(service.execute(new UserLogoutCommand())).toBeTruthy();
    expect(userSession.getLoggedUser()).toBeFalsy();
  });
});

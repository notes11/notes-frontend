import {Command} from "../../../../../core/cqrs/command/command";

export class UserRegisterCommand implements Command {

  public username: string;
  public password: string;
  public passwordRepeat: string;
  public addressEmail: string;

}

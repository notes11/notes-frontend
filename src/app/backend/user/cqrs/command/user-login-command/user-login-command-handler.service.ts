import {Injectable} from "@angular/core";
import {AbstractCommandHandler} from "../../../../../core/cqrs/command/handler/abstract-command-handler.service";
import {UserLoginCommand} from "./user-login-command";
import {UserLoginCommandResult} from "./user-login-command-result";
import {Observable} from "rxjs";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {UserSessionService} from "../../../../../core/service/user-session/user-session.service";
import {map} from "rxjs/operators";
import {User} from "../../../../model/dto/user";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export class UserLoginCommandHandlerService extends AbstractCommandHandler<UserLoginCommand, UserLoginCommandResult> {

  private apiBaseLoginPath: string;
  private userSession: UserSessionService;

  constructor(httpClient: HttpClient, userSession: UserSessionService, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
    this.userSession = userSession;
    this.apiBaseLoginPath = this.configurationService.getConfiguration().backend.apiMappings.login;
  }

  execute(command: UserLoginCommand): Observable<UserLoginCommandResult> {
    return this.httpClient.post<any>(this.apiBaseUriWithBasePath + this.apiBaseLoginPath, command, {observe: "response"})
      .pipe(map(response => {
        const user: UserLoginCommandResult = this.getUserFromFullResponse(response);
        this.userSession.login(user);
        return user;
      }));
  }

  private getUserFromFullResponse(response: HttpResponse<any>): User {
    const user: UserLoginCommandResult = response.body;
    user.jwtToken = this.getAuthorizationHeaderForFullResponse(response);
    return user;
  }

  // noinspection JSMethodCanBeStatic
  private getAuthorizationHeaderForFullResponse(response: HttpResponse<any>): string {
    return response.headers.get("authorization");
  }

}

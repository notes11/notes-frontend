import {User} from "../../../model/dto/user";
import {CommandResult} from "../../../../core/cqrs/command/result/command-result";

export class UserCommandResult extends User implements CommandResult {
}

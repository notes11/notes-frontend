import {CommandResult} from "../../../../../core/cqrs/command/result/command-result";
import {User} from "../../../../model/dto/user";

export class UserUpdateCommandResult extends  User implements CommandResult {
}

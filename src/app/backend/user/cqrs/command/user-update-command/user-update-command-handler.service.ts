import { Injectable } from "@angular/core";
import {AbstractCommandHandler} from "../../../../../core/cqrs/command/handler/abstract-command-handler.service";
import {UserUpdateCommandResult} from "./user-update-command-result";
import {UserUpdateCommand} from "./user-update-command";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export class UserUpdateCommandHandlerService extends AbstractCommandHandler<UserUpdateCommand, UserUpdateCommandResult>{

  private readonly apiBaseUserPath: string;

  constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
    this.apiBaseUserPath = this.configurationService.getConfiguration().backend.apiMappings.user;
  }

  execute(command: UserUpdateCommand): Observable<UserUpdateCommandResult> {
    return this.httpClient.patch<UserUpdateCommandResult>(this.apiBaseUriWithBasePath + this.apiBaseUserPath, command);
  }
}

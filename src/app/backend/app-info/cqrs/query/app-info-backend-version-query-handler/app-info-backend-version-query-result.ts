import {QueryResult} from "../../../../../core/cqrs/query/result/query-result";

export class AppInfoBackendVersionQueryResult implements QueryResult {

  public version: string;

}

import {Injectable} from "@angular/core";
import {AbstractQueryHandler} from "../../../../../core/cqrs/query/handler/abstract-query-handler.service";
import {AppInfoBackendVersionQuery} from "./app-info-backend-version-query";
import {AppInfoBackendVersionQueryResult} from "./app-info-backend-version-query-result";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export class AppInfoBackendVersionQueryHandlerService
  extends AbstractQueryHandler<AppInfoBackendVersionQuery, AppInfoBackendVersionQueryResult> {

  private readonly apiAppInfoVersionUrl;

  constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
    this.apiAppInfoVersionUrl = this.configurationService.getConfiguration().backend.apiMappings.appInfo.version;
  }

  execute(query: AppInfoBackendVersionQuery): Observable<AppInfoBackendVersionQueryResult> {
    return this.httpClient.get<AppInfoBackendVersionQueryResult>(`${this.apiBaseUriWithBasePath}${this.apiAppInfoVersionUrl}`);
  }
}

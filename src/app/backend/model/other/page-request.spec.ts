import { Direction } from "./direction";
import { PageRequest } from "./page-request";
import {pageResponse, pageParams} from "../../../../test/data/test-data.spec";
import {Page} from "./page";

describe("PageRequest model test", () => {
  let pageRequest: PageRequest;

  beforeEach(() => {
    pageRequest = new PageRequest();
  });

  it("should be created", () => {
    expect(pageRequest).toBeTruthy();
  });

  it("should be created with params", () => {
    // given & when
    const pageRequestWithParams = new PageRequest(pageParams);
    // then
    expect(pageRequestWithParams).toBeTruthy();
    expect(pageRequestWithParams.page).toBe(pageParams._page);
    expect(pageRequestWithParams.size).toBe(pageParams._size);
    expect(pageRequestWithParams.direction).toBe(pageParams._direction);
  });

  it("should has default value 'page'", () => {
    expect(pageRequest.page).toEqual(0);
  });

  it("should has default value 'size'", () => {
    expect(pageRequest.size).toBeTruthy();
    expect(pageRequest.size).toEqual(5);
  });

  it("should has default value 'direction'", () => {
    expect(pageRequest.direction).toBeTruthy();
    expect(pageRequest.direction).toEqual(Direction.DESC);
  });

  it("should switch to next page", () => {
    pageRequest.nextPage();
    expect(pageRequest.page).toEqual(1);
  });

  it("should switch to next page - with page param", () => {
    // given
    const page: Page<any> = new Page();
    // eslint-disable-next-line id-blacklist
    page.totalPages = 1;
    // when
    pageRequest.nextPage(page);
    pageRequest.nextPage(page);
    // then
    expect(pageRequest.page).toEqual(0);
  });

  it("should switch to previous page", () => {
    // given
    pageRequest = new PageRequest(pageParams);
    // when
    pageRequest.previousPage();
    // then
    expect(pageRequest.page).toEqual(0);
  });

  it("should change page", () => {
    // given
    const newPage: number = 2;
    pageRequest = new PageRequest(pageParams);
    // when
    pageRequest.changePage(newPage, pageResponse);
    // then
    expect(pageRequest.page).toEqual(newPage);
  });

  it("should change page below zero", () => {
    // given
    const newPage: number = -1;
    pageRequest = new PageRequest(pageParams);
    // when
    pageRequest.changePage(newPage, pageResponse);
    // then
    expect(pageRequest.page).toEqual(0);
  });

  it("should change page with no parameters", () => {
    // given
    pageRequest = new PageRequest(pageParams);
    // when
    pageRequest.changePage();
    // then
    expect(pageRequest.page).toEqual(0);
  });

  it("should change page with no page parameter", () => {
    // given
    const newPage: number = 2;
    pageRequest = new PageRequest(pageParams);
    // when
    pageRequest.changePage(newPage);
    // then
    expect(pageRequest.page).toEqual(newPage);
  });

  it("should change page with new page bigger than totalPages", () => {
    // given
    const newPage: number = pageResponse.totalPages + 1;
    pageRequest = new PageRequest(pageParams);
    // when
    pageRequest.changePage(newPage, pageResponse);
    // then
    expect(pageRequest.page).toEqual(pageResponse.totalPages);
  });

  it("should change page size", () => {
    // given
    const newPageSize = pageResponse.size + 1;
    // when
    pageRequest.changePageSize(newPageSize, pageResponse);
    // then
    expect(pageRequest.size).toEqual(newPageSize);
  });

});

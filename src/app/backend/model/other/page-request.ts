import {Params} from "@angular/router";
import {Direction} from "./direction";
import {Page} from "./page";

export class PageRequest {
  protected _page: number = 0;
  protected _size: number = 5;
  protected _direction: Direction = Direction.DESC;

  constructor(params?: Params) {
    if (params) {
      if (Number(params._page)) {
        this._page = params._page;
      }

      if (Number(params._size)) {
        this._size = params._size;
      }

      if (params._direction) {
        this._direction = params._direction;
      }
    }
  }

  get page(): number {
    return this._page;
  }

  get size(): number {
    return this._size;
  }

  get direction(): Direction {
    return this._direction;
  }

  public changePage(pageNumber?: number, page?: Page<any>): void {
    if (pageNumber !== undefined) {
      if (pageNumber < 0) {
        this._page = 0;
      } else {
        if (page) {
          if (pageNumber > page.totalPages) {
            this._page = page.totalPages;
          } else {
            this._page = pageNumber;
          }
        } else {
          this._page = pageNumber;
        }
      }
    } else {
      this._page = 0;
    }
  }

  public changePageSize(pageSize?: number, page?: Page<any>): void {
    if (pageSize) {
      this._size = pageSize;
    }
    this.changePage(this.page, page);
  }

  public nextPage(page?: Page<any>): void {
    this._page++;
    if (page && page.totalPages !== undefined && this._page > page.totalPages) {
      if (page.totalPages <= 0) {
        this._page = 0;
      } else {
        this._page = page.totalPages - 1;
      }
    }
  }

  public previousPage(): void {
    if (this._page > 0) {
      this._page--;
    }
  }

}

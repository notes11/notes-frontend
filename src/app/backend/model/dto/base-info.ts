export abstract class BaseInfo {

  public uuid: string;
  public createdBy: string;
  public createdTimestamp: Date;
  public modifiedBy: string;
  public modificationTimestamp: Date;

}

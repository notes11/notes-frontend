import {BaseInfo} from "./base-info";
import {Role} from "./role";

export class User extends BaseInfo {

  public username: string;
  public firstName: string;
  public lastName: string;
  public addressEmail: string;
  public isActive: boolean;
  public roles: Role[];
  public jwtToken: string;

}

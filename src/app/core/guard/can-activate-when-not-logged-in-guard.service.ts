import {Injectable} from "@angular/core";
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from "@angular/router";
import {Observable} from "rxjs";
import {UserSessionService} from "../service/user-session/user-session.service";

@Injectable({
  providedIn: "root"
})
export class CanActivateWhenNotLoggedInGuard implements CanActivate {

  private router: Router;
  private userSession: UserSessionService;

  public constructor(router: Router, userSession: UserSessionService) {
    this.router = router;
    this.userSession = userSession;
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (this.userSession.getLoggedUser() === null) {
      return true;
    } else {
      // noinspection JSIgnoredPromiseFromCall
      this.router.navigateByUrl("");
      return false;
    }


  }

}

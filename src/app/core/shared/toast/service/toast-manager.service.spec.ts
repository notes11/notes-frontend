import { TestBed } from "@angular/core/testing";

import { ToastManagerService } from "./toast-manager.service";
import {Toast} from "../model/toast";

describe("ToastManagerService", () => {
  let service: ToastManagerService;
  const messageBody: string = "Some message";

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ToastManagerService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should execute correctly showPrimary", () => {
    service.showPrimary(messageBody);
    expect(service.toasts[0].clazz).toEqual("alert-primary");
    expect(service.toasts[0].body).toEqual(messageBody);
  });

  it("should execute correctly showWarning", () => {
    service.showInfo(messageBody);
    expect(service.toasts[0].clazz).toEqual("alert-info");
    expect(service.toasts[0].body).toEqual(messageBody);
  });

  it("should execute correctly showSuccess", () => {
    service.showSuccess(messageBody);
    expect(service.toasts[0].clazz).toEqual("alert-success");
    expect(service.toasts[0].body).toEqual(messageBody);
  });

  it("should execute correctly showDanger", () => {
    service.showDanger(messageBody);
    expect(service.toasts[0].clazz).toEqual("alert-danger");
    expect(service.toasts[0].body).toEqual(messageBody);
  });

  it("should execute correctly showWarning", () => {
    service.showWarning(messageBody);
    expect(service.toasts[0].clazz).toEqual("alert-warning");
    expect(service.toasts[0].body).toEqual(messageBody);
  });

  it("should execute correctly remove", () => {
    service.showWarning(messageBody);
    const toast: Toast = service.toasts[0];
    service.remove(toast);
    expect(service.toasts.length).toEqual(0);
  });
});

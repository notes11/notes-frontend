import {Injectable} from "@angular/core";
import {Toast} from "../model/toast";

@Injectable({
  providedIn: "root"
})
export class ToastManagerService {

  private _toasts: Toast[] = [];

  get toasts(): Toast[] {
    return this._toasts;
  }

  public remove(toast: Toast): void {
    this._toasts = this._toasts.filter(t => t !== toast);
  }

  public showPrimary(body: string): void {
    this._toasts.push(new Toast("alert-primary", "Notification", new Date(Date.now()), body));
  }

  public showInfo(body: string): void {
    this._toasts.push(new Toast("alert-info", "Info", new Date(Date.now()), body));
  }

  public showSuccess(body: string): void {
    this._toasts.push(new Toast("alert-success", "Success", new Date(Date.now()), body));
  }

  public showDanger(body: string): void {
    this._toasts.push(new Toast("alert-danger", "Error", new Date(Date.now()), body));
  }

  public showWarning(body: string): void {
    this._toasts.push(new Toast("alert-warning", "Warning", new Date(Date.now()), body));
  }

}

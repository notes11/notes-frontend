import {Event} from "../event";
import {EventResult} from "../result/event-result";
import {Observable} from "rxjs";

export interface EventHandler<E extends Event, R extends EventResult> {

  execute(event: E): Observable<R> | R;

}

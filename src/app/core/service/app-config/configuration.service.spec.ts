import { TestBed } from "@angular/core/testing";

import { ConfigurationService } from "./configuration.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {Configuration} from "./configuration";
import {environment} from "../../../../environments/environment";

describe("ConfigurationService", () => {
  let service: ConfigurationService;
  let httpMock: HttpTestingController;
  const configurationUrl: string = environment.urlToAppConfig;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpMock = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ConfigurationService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should load configuration", () => {
    service.loadConfig();
    expect(service.getConfiguration().environment).toBeTruthy();
    expect(service.getConfiguration().environment).toEqual("local");

    const request = httpMock.expectOne(configurationUrl);
    expect(request.request.method).toBe("GET");
    request.flush(new Configuration());
  });

  it("should contain environment", () => {
    expect(service.getConfiguration().environment).toBeTruthy();
    expect(service.getConfiguration().environment).toEqual("local");
  });
});

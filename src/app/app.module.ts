import {BrowserModule} from "@angular/platform-browser";
import {APP_INITIALIZER, NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {AppComponent} from "./app.component";
import {TopNavbarComponent} from "./frontend/shared-component/top-navbar/top-navbar.component";

import {appRoutes} from "./routes";
import {FooterComponent} from "./frontend/shared-component/footer/footer.component";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {JwtTokenInterceptor} from "./core/interceptor/jwt-token-interceptor/jwt-token.interceptor";
import {UserRegisterComponent} from "./frontend/view/user/user-register/user-register.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserLoginComponent} from "./frontend/view/user/user-login/user-login.component";
import {StatusComponent} from "./frontend/shared-component/status/status.component";
import {MainComponent} from "./frontend/view/main/main.component";
import {UserProfileComponent} from "./frontend/view/user/user-profile/user-profile.component";
import {ToastComponent} from "./core/shared/toast/component/toast.component";
import {NgbToastModule} from "@ng-bootstrap/ng-bootstrap";
import {UserIdleModule} from "angular-user-idle";
import {environment} from "../environments/environment";
import {NoteListComponent} from "./frontend/view/note/note-list/note-list.component";
import {ConfigurationService} from "./core/service/app-config/configuration.service";
import {appInitializerFunction} from "./core/service/app-config/app-initializer-function";
import { DatePipe } from "@angular/common";
import { NoteComponent } from "./frontend/view/note/note/note.component";
import { Error401Interceptor } from "./core/interceptor/error-401-interceptor/error-401.interceptor";

@NgModule({
  declarations: [
    AppComponent,
    TopNavbarComponent,
    FooterComponent,
    UserRegisterComponent,
    UserLoginComponent,
    StatusComponent,
    MainComponent,
    UserProfileComponent,
    ToastComponent,
    NoteListComponent,
    NoteComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, {enableTracing: false}),
    NgbToastModule,
    UserIdleModule.forRoot({
      idle: environment.userIdleConfiguration.idle,
      timeout: environment.userIdleConfiguration.timeout,
      ping: environment.userIdleConfiguration.ping
    })
  ],
  providers: [
    DatePipe,
    ConfigurationService,
    {provide: APP_INITIALIZER, useFactory: appInitializerFunction, multi: true, deps: [ConfigurationService]},
    {provide: HTTP_INTERCEPTORS, useClass: JwtTokenInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: Error401Interceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

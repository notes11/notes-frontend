export const environment = {
  production: true,
  urlToAppConfig: "app-config.json",
  appMapping: {
    index: "",
    user: {
      register: "user/register",
      login: "user/login",
      logout: "user/logout",
      profile: "user/profile"
    },
    note: {
      note: "note",
      noteList: "note/my-notes"
    },
    status: "status"
  },
  userIdleConfiguration: {
    idle: 300,
    timeout: 60,
    ping: 10
  }
};

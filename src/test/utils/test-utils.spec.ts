import {User} from "../../app/backend/model/dto/user";
import {Note} from "../../app/backend/model/dto/note";

export const userExpectToBe = (actual: User, expected: User): void => {
  // noinspection DuplicatedCode
  expect(actual.uuid).toBe(expected.uuid);
  expect(actual.createdBy).toBe(expected.createdBy);
  expect(new Date(actual.createdTimestamp)).toEqual(expected.createdTimestamp);
  expect(actual.modifiedBy).toBe(expected.modifiedBy);
  expect(actual.modificationTimestamp).toBe(expected.modificationTimestamp);
  expect(actual.username).toBe(expected.username);
  expect(actual.firstName).toBe(expected.firstName);
  expect(actual.lastName).toBe(expected.lastName);
  expect(actual.addressEmail).toBe(expected.addressEmail);
  expect(actual.isActive).toBe(expected.isActive);
  expect(actual.roles).toEqual(expected.roles);
  expect(actual.jwtToken).toBe(expected.jwtToken);
};

export const noteExpectToBe = (actual: Note, expected: Note): void => {
  // noinspection DuplicatedCode
  expect(actual.uuid).toBe(expected.uuid);
  expect(actual.createdBy).toBe(expected.createdBy);
  expect(actual.createdTimestamp).toBe(expected.createdTimestamp);
  expect(actual.modifiedBy).toBe(expected.modifiedBy);
  expect(actual.modificationTimestamp).toBe(expected.modificationTimestamp);
  expect(actual.title).toBe(expected.title);
  expect(actual.noteText).toBe(expected.noteText);
  expect(actual.owner).toBe(expected.owner);
};
